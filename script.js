let torre = document.getElementsByClassName("torre")
let torreA = document.getElementById('torreA');
let torreB = document.getElementById('torreB');
let torreC = document.getElementById('torreC');

let disco1
let disco2
let discoAtual
let discoSel = false

torreA.onclick = function () {
    clicked(event);
}

torreB.onclick = function () {
    clicked(event);
}

torreC.onclick = function () {
    clicked(event);
    const count = torreC.childElementCount;
    if (count == 4) {
        alert("VOCÊ VENCEU!!")
    }
}

function clicked (event) {
    if (discoSel) {
        disco2 = event.currentTarget;
        moverDisco()

    } else {
        disco1 = event.currentTarget;
        discoAtual = disco1.lastElementChild;
        disco1.removeChild(disco1.lastElementChild);
    }
    discoSel = !discoSel;
}

function moverDisco() {
    if (disco2.childElementCount > 0) {
        if (discoAtual.id.slice(1) < disco2.lastElementChild.id.slice(1)) {
            disco2.appendChild(discoAtual)
        } else {
            disco1.appendChild(discoAtual)
        }
    } else {
        disco2.appendChild(discoAtual)
    }
}